import React from "react";
import {View, Text, StyleSheet, Image} from "react-native";

const ImageDetails = ({title, image}) => {
  return (
    <View>
      <Image styles={styles.image} source={image} />
      <Text>{title}</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  image: {
    maxHeight: 20,
    maxWidth: "100%"
  }
});

export default ImageDetails;
