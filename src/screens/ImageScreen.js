import React from "react";
import {View, Text, StyleSheet} from "react-native";
import ImageDetails from "../Components/ImageDetails";

const ImageScreen = () => {
  const data = [
    {
      title: "Forest",
      image: require("../../assets/forest.jpg")
    },
    {
      title: "Beach",
      image: require("../../assets/beach.jpg")
    },
    {
      title: "Mountain",
      image: require("../../assets/mountain.jpg")
    }
  ];

  return (
    <View>
      {data.map((item, index) => (
        <ImageDetails key={index} title={item.title} image={item.image} />
      ))}
    </View>
  );
};

const styles = StyleSheet.create({});

export default ImageScreen;
