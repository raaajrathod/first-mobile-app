import React from "react";
import {Text, StyleSheet, View, Button, TouchableOpacity} from "react-native";

const HomeScreen = ({navigation: {navigate}}) => {
  // console.log(props.navigation);
  return (
    <View>
      <Text style={styles.text}>Helloo World!</Text>
      <Button
        title='Go To Components Demo'
        onPress={() => {
          navigate("Components");
        }}
      />

      <Button
        title='Go To List Demo'
        onPress={() => {
          navigate("List");
        }}
      />

      <Button
        title='Go To Image Screen'
        onPress={() => {
          navigate("Image");
        }}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  text: {
    fontSize: 30
  },
  buttonStyle: {
    textAlign: "center",
    height: 30,
    backgroundColor: "yellow"
  }
});

export default HomeScreen;
