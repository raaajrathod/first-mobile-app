import React from "react";
import PropTypes from "prop-types";
import {Text, StyleSheet, View} from "react-native";

const ComponentsScreen = props => {
  return (
    <View>
      <Text style={styles.textStyle}>This is ComponentsScreen </Text>
      <Text style={styles.textStyle}>Hey There, Whats Up?</Text>
      <Text style={styles.textStyle}>Hey There, Whats Up?</Text>
    </View>
  );
};

ComponentsScreen.propTypes = {};

const styles = StyleSheet.create({
  textStyle: {
    fontSize: 25
  }
});

export default ComponentsScreen;
